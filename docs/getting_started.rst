Getting Started
===============

Installation
------------

The best way to install is with pip:

.. code-block:: sh

   pip install octranspo
   # or
   python -m pip install octranspo

Alternatively, clone the repository and run:

.. code-block:: sh

   python setup.py install

Use
---

To get started, create an instance of the OCTranspo class:

.. code-block:: python

   from octranspo import OCTranspo

   api = OCTranspo('app_id', 'api_key')

You can also set your App ID and API Key with the environment variables ``OC_APP_ID`` and ``OC_API_KEY``.

Methods will then be available to interact with the OC Transpo API.

.. code-block:: python

   stop_info = api.get_route_summary('3009')

   print(stop_info.stop_label) # Prints 'RIDEAU'
   for route in stop_info.routes:
       print(f'Route {route.route_number} heading to {route.route_heading}')
       # Example output: Route 97 heading to Airport

Convert to dictionary
---------------------

To convert a returned class to a dictionary, you can use ``dataclasses.asdict``
from the standard library:

.. code-block:: python

   from dataclasses import asdict

   # Recursively turns all classes into dictionaries
   stop_info = asdict(api.get_route_summary('3009'))

Now, you can serialize the return value. For example:

.. code-block:: python

   import json

   stop_info_str = json.dumps(stop_info) # String JSON of return value
