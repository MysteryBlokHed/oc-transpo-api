"""Mock data for GTFS API responses"""
GTFS_AGENCY = """
{
  "Query": { "table": "agency", "direction": "ASC", "format": "json" },
  "Gtfs": [
    {
      "id": "1",
      "agency_name": "OC Transpo",
      "agency_url": "http:\\/\\/www.octranspo.com",
      "agency_timezone": "America\\/Montreal",
      "agency_lang": "",
      "agency_phone": "\\r"
    }
  ]
}
"""

GTFS_CALENDAR = """
{
  "Query": { "table": "calendar", "direction": "ASC", "format": "json" },
  "Gtfs": [
    {
      "id": "1",
      "service_id": "SEPT21-d1930Mid-Weekday-03",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211201",
      "end_date": "20211201"
    },
    {
      "id": "2",
      "service_id": "SEPT21-w1900Mid-Weekday-05",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211204",
      "end_date": "20211207"
    },
    {
      "id": "3",
      "service_id": "SEPT21-w1300Mid-Weekday-06",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211211",
      "end_date": "20211211"
    },
    {
      "id": "4",
      "service_id": "SEPT21-MThCFD-Weekday-15",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211117",
      "end_date": "20211216"
    },
    {
      "id": "5",
      "service_id": "SEPT21-FRICFD-Weekday-15",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211119",
      "end_date": "20211217"
    },
    {
      "id": "6",
      "service_id": "SEPT21-SATCFD-Saturday-15",
      "monday": "0",
      "tuesday": "0",
      "wednesday": "0",
      "thursday": "0",
      "friday": "0",
      "saturday": "1",
      "sunday": "0",
      "start_date": "20211120",
      "end_date": "20211218"
    },
    {
      "id": "7",
      "service_id": "SEPT21-SUNCFD-Sunday-15",
      "monday": "0",
      "tuesday": "0",
      "wednesday": "0",
      "thursday": "0",
      "friday": "0",
      "saturday": "0",
      "sunday": "1",
      "start_date": "20211121",
      "end_date": "20211212"
    },
    {
      "id": "8",
      "service_id": "SEPT21-SEPDA21-Weekday-08",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211117",
      "end_date": "20211217"
    },
    {
      "id": "9",
      "service_id": "SEPT21-SEPSA21-Saturday-02",
      "monday": "0",
      "tuesday": "0",
      "wednesday": "0",
      "thursday": "0",
      "friday": "0",
      "saturday": "1",
      "sunday": "0",
      "start_date": "20211120",
      "end_date": "20211218"
    },
    {
      "id": "10",
      "service_id": "SEPT21-SEPSU21-Sunday-02",
      "monday": "0",
      "tuesday": "0",
      "wednesday": "0",
      "thursday": "0",
      "friday": "0",
      "saturday": "0",
      "sunday": "1",
      "start_date": "20211121",
      "end_date": "20211212"
    },
    {
      "id": "11",
      "service_id": "SEPT21-303Shop-Weekday-01",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211117",
      "end_date": "20211215"
    },
    {
      "id": "12",
      "service_id": "SEPT21-304Shop-Weekday-01",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211118",
      "end_date": "20211216"
    },
    {
      "id": "13",
      "service_id": "SEPT21-305Shop-Weekday-01",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211119",
      "end_date": "20211217"
    },
    {
      "id": "14",
      "service_id": "SEPT21-301Shop-Weekday-01",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211122",
      "end_date": "20211213"
    },
    {
      "id": "15",
      "service_id": "SEPT21-302Shop-Weekday-01",
      "monday": "1",
      "tuesday": "1",
      "wednesday": "1",
      "thursday": "1",
      "friday": "1",
      "saturday": "0",
      "sunday": "0",
      "start_date": "20211123",
      "end_date": "20211214"
    }
  ]
}
"""
