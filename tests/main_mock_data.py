"""Mock data for main API responses"""
ROUTE_SUMMARY_RESPONSE = """
{
  "GetRouteSummaryForStopResult": {
    "StopNo": "3009",
    "Error": "",
    "StopDescription": "RIDEAU",
    "Routes": {
      "Route": [
        {
          "RouteNo": "R1",
          "RouteHeading": "Tunney's Pasture",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "R1",
          "RouteHeading": "Blair",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "1",
          "RouteHeading": "Blair",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "1",
          "RouteHeading": "Tunney's Pasture",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "5",
          "RouteHeading": "Billings Bridge",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "5",
          "RouteHeading": "Rideau",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "6",
          "RouteHeading": "Greenboro",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "6",
          "RouteHeading": "Rockcliffe",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "7",
          "RouteHeading": "Carleton",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "7",
          "RouteHeading": "St-Laurent",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "9",
          "RouteHeading": "Hurdman",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "14",
          "RouteHeading": "St-Laurent",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "14",
          "RouteHeading": "Tunney's Pasture",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "15",
          "RouteHeading": "Blair",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "15",
          "RouteHeading": "Parliament",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "18",
          "RouteHeading": "St-Laurent",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "18",
          "RouteHeading": "Parliament",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "39",
          "RouteHeading": "Blair & N Rideau",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "39",
          "RouteHeading": "Millennium",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "45",
          "RouteHeading": "Hospital",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "57",
          "RouteHeading": "Crystal Bay",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "61",
          "RouteHeading": "Tunney's Pasture & N Rideau & Gatineau",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "61",
          "RouteHeading": "Stittsville",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "75",
          "RouteHeading": "Tunney's Pasture & Gatineau & N Rideau",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "75",
          "RouteHeading": "Barrhaven Centre",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "97",
          "RouteHeading": "Airport",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "114",
          "RouteHeading": "Carlington",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "450",
          "RouteHeading": "Lansdowne",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "602",
          "RouteHeading": "De La Salle",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "615",
          "RouteHeading": "Parliament",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "616",
          "RouteHeading": "Gloucester H.S",
          "Direction": "",
          "DirectionID": 0
        },
        {
          "RouteNo": "616",
          "RouteHeading": "Parliament",
          "Direction": "",
          "DirectionID": 1
        },
        {
          "RouteNo": "624",
          "RouteHeading": "Gloucester H.S",
          "Direction": "",
          "DirectionID": 0
        }
      ]
    }
  }
}
"""

NEXT_TRIPS_ROUTEDIRECTION_ARRAY = """
{
  "GetNextTripsForStopResult": {
    "StopNo": "3034",
    "StopLabel": "BILLINGS BRIDGE",
    "Error": "",
    "Route": {
      "RouteDirection": [
        {
          "RouteNo": "97",
          "RouteLabel": "Airport",
          "Direction": "",
          "Error": "",
          "RequestProcessingTime": "20211118221159",
          "Trips": {
            "Trip": [
              {
                "Longitude": "",
                "Latitude": "",
                "GPSSpeed": "",
                "TripDestination": "Airport",
                "TripStartTime": "22:12",
                "AdjustedScheduleTime": "5",
                "AdjustmentAge": "-1",
                "LastTripOfSchedule": false,
                "BusType": ""
              },
              {
                "Longitude": "",
                "Latitude": "",
                "GPSSpeed": "",
                "TripDestination": "Airport",
                "TripStartTime": "22:27",
                "AdjustedScheduleTime": "20",
                "AdjustmentAge": "-1",
                "LastTripOfSchedule": false,
                "BusType": ""
              },
              {
                "Longitude": "",
                "Latitude": "",
                "GPSSpeed": "",
                "TripDestination": "Airport",
                "TripStartTime": "22:41",
                "AdjustedScheduleTime": "34",
                "AdjustmentAge": "-1",
                "LastTripOfSchedule": false,
                "BusType": ""
              }
            ]
          }
        },
        {
          "RouteNo": "97",
          "RouteLabel": "Hurdman & N Rideau",
          "Direction": "",
          "Error": "",
          "RequestProcessingTime": "20211118221159",
          "Trips": {
            "Trip": [
              {
                "Longitude": "-75.65839385986328",
                "Latitude": "45.35981369018555",
                "GPSSpeed": "38",
                "TripDestination": "Hurdman & N Rideau",
                "TripStartTime": "22:08",
                "AdjustedScheduleTime": "3",
                "AdjustmentAge": "2.18",
                "LastTripOfSchedule": false,
                "BusType": ""
              },
              {
                "Longitude": "",
                "Latitude": "",
                "GPSSpeed": "",
                "TripDestination": "Hurdman & N Rideau",
                "TripStartTime": "22:19",
                "AdjustedScheduleTime": "17",
                "AdjustmentAge": "-1",
                "LastTripOfSchedule": false,
                "BusType": ""
              },
              {
                "Longitude": "",
                "Latitude": "",
                "GPSSpeed": "",
                "TripDestination": "Hurdman & N Rideau",
                "TripStartTime": "22:38",
                "AdjustedScheduleTime": "32",
                "AdjustmentAge": "-1",
                "LastTripOfSchedule": false,
                "BusType": ""
              }
            ]
          }
        }
      ]
    }
  }
}
"""

NEXT_TRIPS_ROUTEDIRECTION_DICT = """
{
  "GetNextTripsForStopResult": {
    "StopNo": "3009",
    "StopLabel": "RIDEAU",
    "Error": "",
    "Route": {
      "RouteDirection": {
        "RouteNo": "39",
        "RouteLabel": "Millennium",
        "Direction": "",
        "Error": "12",
        "RequestProcessingTime": "20211118221036",
        "Trips": {
          "Trip": [
            {
              "Longitude": "",
              "Latitude": "",
              "GPSSpeed": "",
              "TripDestination": "Millennium",
              "TripStartTime": "01:39",
              "AdjustedScheduleTime": "208",
              "AdjustmentAge": "-1",
              "LastTripOfSchedule": false,
              "BusType": ""
            },
            {
              "Longitude": "",
              "Latitude": "",
              "GPSSpeed": "",
              "TripDestination": "Millennium",
              "TripStartTime": "02:06",
              "AdjustedScheduleTime": "235",
              "AdjustmentAge": "-1",
              "LastTripOfSchedule": false,
              "BusType": ""
            }
          ]
        }
      }
    }
  }
}
"""

NEXT_TRIPS_ALL_ROUTES = """
{
  "GetRouteSummaryForStopResult": {
    "StopNo": "7633",
    "Error": "",
    "StopDescription": "HAWTHORNE \\/ COLONEL BY",
    "Routes": {
      "Route": {
        "RouteNo": "5",
        "RouteHeading": "Rideau",
        "DirectionID": 1,
        "Direction": "",
        "Trips": {
          "Trip": [
            {
              "Longitude": "",
              "Latitude": "",
              "GPSSpeed": "",
              "TripDestination": "Rideau",
              "TripStartTime": "22:34",
              "AdjustedScheduleTime": "20",
              "AdjustmentAge": "-1",
              "LastTripOfSchedule": false,
              "BusType": ""
            },
            {
              "Longitude": "",
              "Latitude": "",
              "GPSSpeed": "",
              "TripDestination": "Rideau",
              "TripStartTime": "23:04",
              "AdjustedScheduleTime": "50",
              "AdjustmentAge": "-1",
              "LastTripOfSchedule": false,
              "BusType": ""
            },
            {
              "Longitude": "",
              "Latitude": "",
              "GPSSpeed": "",
              "TripDestination": "Rideau",
              "TripStartTime": "23:34",
              "AdjustedScheduleTime": "80",
              "AdjustmentAge": "-1",
              "LastTripOfSchedule": false,
              "BusType": ""
            }
          ]
        }
      }
    }
  }
}
"""
