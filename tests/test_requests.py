"""Test API requests"""
import os
import secrets
import unittest
from unittest.mock import patch

from main_mock_data import (
    ROUTE_SUMMARY_RESPONSE,
    NEXT_TRIPS_ROUTEDIRECTION_ARRAY,
    NEXT_TRIPS_ROUTEDIRECTION_DICT,
    NEXT_TRIPS_ALL_ROUTES,
)
from gtfs_mock_data import (
    GTFS_AGENCY,
    GTFS_CALENDAR,
)
from octranspo import OCTranspo  # pylint: disable=import-error

# pylint: disable=missing-class-docstring,missing-function-docstring,no-self-use,unused-variable

GET_METHOD = "octranspo.octranspo.requests.Session.get"

os.environ["OC_APP_ID"] = secrets.token_hex(4)
os.environ["OC_API_KEY"] = secrets.token_hex(16)


class MainRequestsTests(unittest.TestCase):
    def test_create_instance(self):
        environ_api = OCTranspo()
        param_api = OCTranspo(os.environ["OC_APP_ID"], os.environ["OC_API_KEY"])

    @patch(GET_METHOD)
    def test_route_summary(self, mock_get):
        api = OCTranspo()

        mock_get.return_value.status_code = 200
        mock_get.return_value.content = ROUTE_SUMMARY_RESPONSE

        summary = api.get_route_summary("3009")
        self.assertEqual(summary.stop_number, "3009")

    @patch(GET_METHOD)
    def test_next_trips(self, mock_get):
        api = OCTranspo()

        mock_get.return_value.status_code = 200
        mock_get.return_value.content = NEXT_TRIPS_ROUTEDIRECTION_ARRAY

        next_trips_from_array = api.get_next_trips("3034", "97")

        mock_get.return_value.content = NEXT_TRIPS_ROUTEDIRECTION_DICT

        next_trips_no_array = api.get_next_trips("3009", "39")

    @patch(GET_METHOD)
    def test_next_trips_all_routes(self, mock_get):
        api = OCTranspo()

        mock_get.return_value.status_code = 200
        mock_get.return_value.content = NEXT_TRIPS_ALL_ROUTES

        next_trips_all_routes = api.get_next_trips_all_routes("7633")

    @patch(GET_METHOD)
    def test_gtfs_table(self, mock_get):
        api = OCTranspo()

        mock_get.return_value.response_code = 200
        mock_get.return_value.content = GTFS_AGENCY

        api.gtfs_agency()

    @patch(GET_METHOD)
    def test_gtfs_agency(self, mock_get):
        api = OCTranspo()

        mock_get.return_value.response_code = 200
        mock_get.return_value.content = GTFS_AGENCY

        agency = api.gtfs_agency()[0]

    @patch(GET_METHOD)
    def test_gtfs_calendar(self, mock_get):
        api = OCTranspo()

        mock_get.return_value.response_code = 200
        mock_get.return_value.content = GTFS_CALENDAR

        calendar = api.gtfs_calendar()


if __name__ == "__main__":
    unittest.main()
